package model;

public class Boeing747 extends Plane {

    public Boeing747(int weight, String modelName, int flightRange, double engineVolume, double fuel) {
        super(weight, modelName, flightRange, engineVolume, fuel);
    }

    @Override
    public int getPeopleCapacity(String modelName, Plane plane) {
        return super.getPeopleCapacity(modelName, plane);
    }

    @Override
    public int getTotalCapacity(String modelName, Plane plane) {
        return super.getTotalCapacity(modelName, plane);
    }
}
