package model;

public class Airbus380 extends Plane {

    public Airbus380(int weight, String modelName, int flightRange, double engineVolume, double fuel) {
        super(weight, modelName, flightRange, engineVolume, fuel);
    }

    @Override
    public int getPeopleCapacity(String modelName, Plane plane) {
        return super.getPeopleCapacity(modelName, plane);
    }

    @Override
    public int getTotalCapacity(String modelName, Plane plane) {
        return super.getTotalCapacity(modelName, plane);
    }
}
